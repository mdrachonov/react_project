# react_project
Blog na którym można czytać artykuły i dodawać do nich komentarze, usuwać artykuły. Również jest możliwość filtrowania artykułów.
Jest dodana lokalizacja, więc można zmieniać języki.

W projekcie są wykorzystane następujące narzędzia: react, redux, immutable, bootstrap, sass, webpack
