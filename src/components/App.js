import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Articles from './routes/Articles'
import NewArticle from './routes/NewArticle'
import NotFound from './routes/NotFound'
import CommentsPage from './routes/CommentsPage'
import Filters from './Filters'
import 'react-select/dist/react-select.css'
import {Switch, Route, Redirect, NavLink} from 'react-router-dom'
import {ConnectedRouter} from 'react-router-redux'
import LangProvider from './LangProvider'
import history from '../history'
import LocalizedText from './LocalizedText'
import './App.scss'

class App extends Component {
    static propTypes = {
    };

    state = {
        language: 'pl'
    }

    changeLanguage = language => ev => this.setState({ language })

    render() {
        return (
            <ConnectedRouter history = {history}>
                <LangProvider language = {this.state.language}>
                    <div className="container">
                        <div className="language-change">
                            <a onClick = {this.changeLanguage('en')}>English</a>
                            <a onClick = {this.changeLanguage('pl')}>Polish</a>
                        </div>
                        <div>
                            <h2><LocalizedText>Main menu</LocalizedText></h2>
                            <div><NavLink activeStyle={{color: 'red'}} to="/filters"><LocalizedText>Filters</LocalizedText></NavLink></div>
                            <div><NavLink activeStyle={{color: 'red'}} to="/articles"><LocalizedText>Articles</LocalizedText></NavLink></div>
                            <div><NavLink activeStyle={{color: 'red'}} to="/comments"><LocalizedText>All comments</LocalizedText></NavLink></div>
                        </div>
                        <Switch>
                            <Route path="/filters" component={Filters}/>
                            <Route path="/articles/new" component={NewArticle}/>
                            <Route path="/articles" component={Articles}/>
                            <Route path='/comments' component={CommentsPage}/>
                            <Route path="*" component={NotFound}/>
                        </Switch>
                    </div>
                </LangProvider>
            </ConnectedRouter>
        )
    }
}

export default App
