import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {addComment} from '../../actions'
import './style.scss'
import LocalizedText from '../LocalizedText'

class CommentForm extends Component {
    static propTypes = {
        articleId: PropTypes.string.isRequired,
        addComment: PropTypes.func.isRequired
    };

    state = {
        user: '',
        text: ''
    }

    render() {
        return (
            <form onSubmit = {this.handleSubmit} className="comment-add-form">
                <p><LocalizedText>Add comment</LocalizedText></p>
                <div className="form-group">
                    <label for="username">User:</label>
                    <input value = {this.state.user} name = "username"
                             onChange = {this.handleChange('user')}
                             className = "form-control" />
                </div>
                <div className="form-group">
                    <label for="comment-text">Comment:</label>
                    <input value = {this.state.text} name = "comment-text"
                                onChange = {this.handleChange('text')}
                                className = "form-control" />
                </div>
                <button type = "submit" className="btn btn-primary">Submit</button>
            </form>
        )
    }

    handleSubmit = ev => {
        ev.preventDefault()
        
        this.props.addComment(this.state)
        this.setState({
            user: '',
            text: ''
        })
    }

    handleChange = type => ev => {
        const {value} = ev.target

        if (value.length > limits[type].max) return

        this.setState({
            [type]: value
        })
    }
}

const limits = {
    user: {
        min: 5,
        max: 15
    },
    text: {
        min: 20,
        max: 50
    }
}

export default connect(null, (dispatch, ownProps) => ({
    addComment: (comment) => dispatch(addComment(comment, ownProps.articleId))
}))(CommentForm)
