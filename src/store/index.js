import {createStore, applyMiddleware} from 'redux'
import reducer from '../reducer'
import randomId from '../middlewares/randomId'
import api from '../middlewares/api'
import thunk from 'redux-thunk'
import {routerMiddleware} from 'react-router-redux'
import history from '../history'

const enhancer = applyMiddleware(thunk, routerMiddleware(history), randomId, api)

const store = createStore(reducer, {}, enhancer)

window.store = store

export default store
