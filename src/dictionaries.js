export const en = {
    'delete me': 'Delete article',
    'show comments': 'Show comments',
    'hide comments': 'Hide comments',
    'No comments yet': 'No comments yet',
    'Loading': 'Loading',
    'Please select article': 'Select article',
    'Main menu': 'Menu'
}

export const pl = {
    'delete me': 'Usuń artykuł',
    'show comments': 'Pokaż komentarze',
    'hide comments': 'Ukryj komentarze',
    'Loading': 'Ładowanie',
    'No comments yet': 'Na razie nie ma komentarzy',
    'Please select article': 'Wybierz artykuł',
    'Name': 'Nazwa',
    'Filters': 'Filtry',
    'Articles': ' Artykuły',
    'Main menu': 'Menu',
    'Comments': 'Komentarze',
    'All comments': 'Wszystkie komentarze'
}

export default { pl, en }
